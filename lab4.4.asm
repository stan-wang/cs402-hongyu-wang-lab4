.data
msg: .asciiz "the largest number is: "
.text
.gloabl main
main:
    li $v0, 5
    syscall
    move $t0, $v0
    li $v0, 5
    syscall
    move $t1, $v0
    addi $sp, $sp, -4
    sw $ra, 4($sp)
    sw $t1, 8($sp)
    jal largest
    addi $sp, $sp, 8
    lw $ra, 4($sp)
    addi $sp, $sp, 4
    jr $ra
largest:
    li $v0, 4
    la $a0, msg
    syscall
    lw $a0, 4($sp)
    lw $a1, 8($sp)
    slt $t0, $a0, $a1
    blez $t0, output
    move $a0, $a1
output:
    li $v0, 1
    syscall
    jr $ra