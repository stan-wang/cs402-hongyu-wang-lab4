.data
input_msg: .asciiz "input number: "
output_msg: .asciiz "factorial of input number is: "
err_msg: .asciiz "input is negative. try again. \n"

.text
.globl main
main:
    li $v0, 4
    la $a0, input_msg
    syscall
    li $v0, 5
    syscall
    move $t0, $v0
    bgez $t0, next
    li $v0, 4
    la $a0, err_msg
    syscall
    j main
next:
    addi $sp, $sp, -4
    sw $ra, 4($sp)
    move $a0, $t0
    jal factorial
    move $t0, $v0
    li $v0, 4
    la $a0, output_msg
    syscall
    li $v0, 1
    move $a0, $t0
    syscall
    lw $ra, 4($sp)
    addi $sp, $sp, 4
    jr $ra
factorial:
    addi $sp, $sp, -4
    sw $ra, 4($sp)
    beqz $a0, terminate
    addi $sp, $sp, -4
    sw $a0, 4($sp)
    sub $a0, $a0, 1
    jal factorial
    lw $t0, 4($sp)
    mul $v0, $v0, $t0
    lw $ra, 8($sp)
    addi $sp, $sp, 8
    jr $ra
terminate:
    li $v0, 1
    lw $ra, 4($sp)
    addi $sp, $sp, 4
    jr $ra